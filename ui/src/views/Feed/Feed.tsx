import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'state/reducers';
import { Post as IPost } from 'models/post';
import { Post } from 'components/Post/Post';
import { fetchFeed, submitNewPost } from 'state/actions/feed.actions';
import { Button } from 'components/Button/Button';
import './styles.scss';

export function Feed() {

    const dispatch = useDispatch();
    const feed = useSelector((state: AppState) => state.feed);

    const [message, setMessage] = useState('');

    useEffect(() => {
        dispatch(fetchFeed())
    }, [dispatch])


    return feed.loading ? (
        <div>
            Loading...
        </div>
    ) : (
        <div className="feed">
            <div className="feed__create">
                <textarea value={message} onChange={(e) => setMessage(e.target.value)}></textarea>
                <Button title="Send Post" onClick={ () => dispatch(submitNewPost( message )) }/>
            </div>
            <div className="feed__posts">
                { feed.feed.map( (post: IPost) => (
                    <Post key={post.id} {...post}/>
                )) }
            </div>
        </div>
    )
}