import React, { useState, useEffect } from 'react';
import {
    Link, useHistory
  } from 'react-router-dom';
import { Input } from 'components/Input/Input';
import { Button } from 'components/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import { authLogin } from 'state/actions/auth.actions';
import { AppState } from 'state/reducers';
import './styles.scss';

export function Login() {

    const history = useHistory();
    const dispatch = useDispatch();
    const store = useSelector((state: AppState) => state.auth);
    
    useEffect(() => {
        if(store.token) {
            history.push('/');
        }
    }, [history, store.token])

    const [ email, setEmail ] = useState('test@test.uk');
    const [ password, setPassword ] = useState('test');

    const handleLogin = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        dispatch(authLogin(email, password));
    }

    return (
        <div className="login">
            <div>LOGIN</div>
            <Input value={email} onChange={(e) => setEmail(e.target.value)}/>
            <Input value={password} onChange={(e) => setPassword(e.target.value)} type="password"/>
            <Button title="Login" onClick={handleLogin}/>
            <div>{store.error}</div>
        </div>
    )
}