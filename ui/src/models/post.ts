import { User } from './user';
import { Like } from './like';

export interface Post {
    id: number;
    message: string;
    created: string;
    user: User;
    likes: Like[];
}