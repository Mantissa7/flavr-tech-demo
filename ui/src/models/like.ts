import { User } from './user';
import { Post } from './post';

export interface Like {
    id: number;
    created: string;
    user_id: number;
    post_id: number;
    // post: Post;
    // user: User;
}