import { fromFetch } from 'rxjs/fetch';

type RequestBody = {[key: string]: any};

export class HttpClient {

    private encode(data: RequestBody) {
        return JSON.stringify(data);
    }

    private fetch(url: string, init: RequestInit) {
        return fromFetch(url, init);
    }

    public get(url: string, init?: RequestInit) {
        return this.fetch(url, {
            method: 'GET',
            ...init,
        });
    }

    public post(url: string, data: RequestBody, init?: RequestInit) {
        return this.fetch(url, {
            method: 'POST',
            ...init,
            body: this.encode(data),
        });
    }

}