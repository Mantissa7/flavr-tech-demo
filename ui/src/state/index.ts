import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { rootEpic } from './epics';
import { rootReducer } from './reducers';
import { ServiceProvider } from 'lib/service-provider';

export const makeStore = ( di: ServiceProvider ) => {
    const epicMiddleware = createEpicMiddleware( {
        dependencies: di,
    } );

    const store = createStore(
        rootReducer,
        applyMiddleware( epicMiddleware )
    );

    epicMiddleware.run( rootEpic );

    return store;
};
