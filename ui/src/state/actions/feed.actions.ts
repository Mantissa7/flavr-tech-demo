import { Action, AnyAction } from 'redux';
import { 
    FETCH_FEED, FETCH_FEED_SUCCESS, FETCH_FEED_FAILED, 
    SUBMIT_NEW_POST, SUBMIT_NEW_POST_SUCCESS, SUBMIT_NEW_POST_FAILED, 
    FETCH_MORE_POSTS, FETCH_MORE_POSTS_SUCCESS, FETCH_MORE_POSTS_FAILED, LIKE_POST, LIKE_POST_SUCCESS, LIKE_POST_FAILED 
} from './types';
import { Post } from 'models/post';

export function fetchFeed(): Action {
    return {
        type: FETCH_FEED,
    }
}

export function fetchFeedSuccess(feed: Post[]): AnyAction {
    return {
        type: FETCH_FEED_SUCCESS,
        payload: {
            feed,
        }
    }
}

export function fetchFeedFailed(reason: string): AnyAction {
    return {
        type: FETCH_FEED_FAILED,
        payload: reason,
    }
}

export function likePost(postID: number): AnyAction {
    return {
        type: LIKE_POST,
        payload: {
            postID,
        }
    }
}

export function likePostSuccess(): Action {
    return {
        type: LIKE_POST_SUCCESS,
    }
}

export function likePostFailed(): Action {
    return {
        type: LIKE_POST_FAILED,
    }
}

export function submitNewPost(message: string): AnyAction {
    return {
        type: SUBMIT_NEW_POST,
        payload: {
            message,
        }
    }
}

export function submitNewPostSuccess(): Action {
    return {
        type: SUBMIT_NEW_POST_SUCCESS,
    }
}

export function submitNewPostFailed(): Action {
    return {
        type: SUBMIT_NEW_POST_FAILED,
    }
}

export function fetchMorePosts(): AnyAction {
    return {
        type: FETCH_MORE_POSTS,
    }
}

export function fetchMorePostsSuccess(posts: Post[]): AnyAction {
    return {
        type: FETCH_MORE_POSTS_SUCCESS,
        payload: {
            posts,
        },
    }
}

export function fetchMorePostsFailed(): Action {
    return {
        type: FETCH_MORE_POSTS_FAILED,
    }
}