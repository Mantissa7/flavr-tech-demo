import { AnyAction } from 'redux';
import { 
    AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILED, 
    AUTH_SIGNUP, AUTH_SIGNUP_SUCCESS, AUTH_SIGNUP_FAILED 
} from './types';

export function authLogin(email: string, password: string): AnyAction {
    return {
        type: AUTH_LOGIN,
        payload: {
            email,
            password
        }
    }
}

export function authLoginSucess(token: string, user: any): AnyAction {
    return {
        type: AUTH_LOGIN_SUCCESS,
        payload: {
            token,
            user,
        }
    }
}

export function authLoginFailed(reason: string): AnyAction {
    return {
        type: AUTH_LOGIN_FAILED,
        payload: {
            reason,
        }
    }
}

export function authSignup(email: string, password: string): AnyAction {
    return {
        type: AUTH_SIGNUP,
        payload: {
            email,
            password
        }
    }
}

export function authSignupSuccess(token: string, user: any): AnyAction {
    return {
        type: AUTH_SIGNUP_SUCCESS,
        payload: {
            token,
            user,
        }
    }
}

export function authSignupFailed(reason: string): AnyAction {
    return {
        type: AUTH_SIGNUP_FAILED,
        payload: {
            reason,
        }
    }
}