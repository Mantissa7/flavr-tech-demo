import { combineEpics } from 'redux-observable';
import { loadFeedEpic, submitPostEpic, fetchMorePostsEpic, likePostEpic } from './feedEpics';
import { loginEpic } from './authEpics';

export const rootEpic = combineEpics(
    loginEpic,
    loadFeedEpic,
    likePostEpic,
    fetchMorePostsEpic,
    submitPostEpic,
);