import { AUTH_LOGIN, AUTH_SIGNUP } from 'state/actions/types';
import { ofType, StateObservable } from 'redux-observable';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { AnyAction } from 'redux';
import { Observable, of, from } from 'rxjs';
import { AuthService } from 'services/auth-service';
import { authLoginFailed, authLoginSucess, authSignupFailed, authSignupSuccess } from 'state/actions/auth.actions';
import { ServiceProvider } from 'lib/service-provider';

export const loginEpic = (action$: Observable<AnyAction>, state$: StateObservable<any>, di: ServiceProvider): Observable<AnyAction> => {
  return action$.pipe(
    ofType(AUTH_LOGIN),
    // withLatestFrom(state$),
    mergeMap((action) => {
      const { email, password } = action.payload;
      const authService: AuthService = di.get('auth-service');
      return from(authService.login(email, password)).pipe(
        mergeMap(resp => {
          if(!/2\d\d/.test(resp.status.toString())) {
            return of(authLoginFailed('Login Failed'))
          }
          return from(resp.json()).pipe(
            map( (resp: any) => {
              localStorage.setItem('access_token', resp.token)
              return authLoginSucess(resp.token, resp.user);
              // return authLoginSucess(resp.user.stsTokenManager.accessToken);
            }), 
          );
        }),
        catchError(error => {
          console.warn(`${error.code}: ${error.message}`);
          return of(authLoginFailed(error.message || 'Login Failed'));
        })

      );
    }),
  )
};

export const authSignupEpic = (action$: Observable<AnyAction>, state$: StateObservable<any>, di: ServiceProvider): Observable<AnyAction> => {
  return action$.pipe(
    ofType(AUTH_SIGNUP),
    // withLatestFrom(state$),
    mergeMap((action) => {
      const { 
        email, 
        password, 
        username, 
        fullname, 
        nickname, 
        photoURL 
      } = action.payload;

      const authService: AuthService = di.get('auth-service');
      return authService.signup(
        email, password, username, fullname, nickname, photoURL
      ).pipe(
        map( (resp: any) => {
          return authSignupSuccess(resp.user, resp.token);
          // return authLoginSucess(resp.user.stsTokenManager.accessToken);
        }), catchError(error => {
          console.warn(`${error.code}: ${error.message}`);
          return of(authSignupFailed(error.message));
        })

      );
    }),
  )
};