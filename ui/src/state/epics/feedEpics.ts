import { FETCH_FEED, SUBMIT_NEW_POST, FETCH_MORE_POSTS, LIKE_POST } from 'state/actions/types';
import { fetchFeedSuccess, fetchFeedFailed, submitNewPostSuccess, submitNewPostFailed, fetchMorePostsSuccess, fetchMorePostsFailed, likePostFailed, fetchFeed } from 'state/actions/feed.actions';
import { ofType } from 'redux-observable';
import { map, mergeMap, catchError, withLatestFrom } from 'rxjs/operators';
import { FeedService } from 'services/feed-service';
import { AnyAction } from 'redux';
import { Observable, of, from } from 'rxjs';
import { AppState } from 'state/reducers';
import { ServiceProvider } from 'lib/service-provider';
import { Post } from 'models/post';

export const loadFeedEpic = (action$: Observable<AnyAction>, state$: Observable<AppState>, di: ServiceProvider): Observable<AnyAction> => {
  return action$.pipe(
    ofType(FETCH_FEED),
    mergeMap(() => {
      const feedService: FeedService = di.get('feed-service');
      return from(feedService.fetchFeed()).pipe(
        mergeMap((resp: Response) => {

          if(!/2\d\d/.test(resp.status.toString())) {
            return of(fetchFeedFailed('Feed Failed to Load'))
          }

          return from(resp.json()).pipe(
            map((resp: Post[]) => {
              return fetchFeedSuccess(resp.sort((a, b) => (new Date(b.created).getTime() - new Date(a.created).getTime()) ));
            })
          )
        }),
        catchError(err => {
          console.warn(err);
          return of(fetchFeedFailed('Feed Failed'));
        })
      );
    }),
  )
};

export const likePostEpic = (action$: Observable<AnyAction>, state$: Observable<AppState>, di: ServiceProvider): Observable<AnyAction> => {
  return action$.pipe(
    ofType(LIKE_POST),
    mergeMap((action) => {
      const { postID } = action.payload;
      const feedService: FeedService = di.get('feed-service');
      return from(feedService.likePost(postID)).pipe(
        mergeMap((resp: Response) => {

          if(!/2\d\d/.test(resp.status.toString())) {
            return of(fetchFeedFailed('Feed Failed to Load'))
          }

          return of(fetchFeed());
        }),
        catchError(err => {
          console.warn(err);
          return of(likePostFailed());
        })
      );
    }),
  )
};

export const fetchMorePostsEpic = (action$: Observable<AnyAction>, state$: Observable<AppState>, di: ServiceProvider): Observable<AnyAction> => {
  return action$.pipe(
    ofType(FETCH_MORE_POSTS),
    withLatestFrom(state$),
    mergeMap(([action, state]: [AnyAction, AppState]) => {
      // const feed = state.feed;
      const feedService: FeedService = di.get('feed-service');
      return from(feedService.fetchFeed()).pipe(
        map(resp => {
          // return fetchMorePostsSuccess(resp);
          return fetchMorePostsSuccess([]);
        }), catchError(err => {
          console.warn(err);
          return of(fetchMorePostsFailed());
        })
      );
    }),
  )
};

export const submitPostEpic = (action$: Observable<AnyAction>, state$: Observable<AppState>, di: ServiceProvider) => {
  return action$.pipe(
    ofType(SUBMIT_NEW_POST),
    withLatestFrom(state$),
    mergeMap(([action, state]: [AnyAction, AppState]) => {
      const userID = state.auth.user.id;
      const { message } = action.payload;
      const feedService: FeedService = di.get('feed-service');


      return from(feedService.submitPost(message, userID)).pipe(
        mergeMap(success => {
          // return submitNewPostSuccess();
          return [
            submitNewPostSuccess(),
            fetchFeed(),
          ];
        }), catchError(err => {
          console.warn(err);
          return of(submitNewPostFailed());
        })
      );


    }),
  )
};