import { AnyAction } from 'redux';
import { 
    FETCH_FEED, FETCH_FEED_SUCCESS, FETCH_FEED_FAILED, 
    SUBMIT_NEW_POST, SUBMIT_NEW_POST_SUCCESS, SUBMIT_NEW_POST_FAILED, 
    FETCH_MORE_POSTS, FETCH_MORE_POSTS_SUCCESS, FETCH_MORE_POSTS_FAILED, LIKE_POST_SUCCESS, LIKE_POST, LIKE_POST_FAILED 
} from 'state/actions/types';
import { Post } from 'models/post';

interface State {
    loading: boolean;
    feed: Post[];
    error: string;
}

const initialState: State = {
    loading: false,
    feed: [],
    error: null,
};

export function feedReducer(state: State = initialState, action: AnyAction) {
    switch(action.type) {
        case FETCH_FEED: {
            return {
                ...state,
                loading: true,
                error: null,
            }
        }
        case FETCH_FEED_SUCCESS: {
            const { feed } = action.payload;
            return {
                ...state,
                loading: false,
                feed,
            }
        }
        case FETCH_FEED_FAILED: {
            const { reason } = action.payload;
            return {
                ...state,
                loading: false,
                error: reason,
            }
        }
        case LIKE_POST: {
            return {
                ...state,
            }
        }
        case LIKE_POST_SUCCESS: {
            return {
                ...state,
            }
        }
        case LIKE_POST_FAILED: {
            return {
                ...state,
            }
        }
        case SUBMIT_NEW_POST: {
            return {
                ...state,
            }
        }
        case SUBMIT_NEW_POST_SUCCESS: {
            return {
                ...state,
            }
        }
        case SUBMIT_NEW_POST_FAILED: {
            return {
                ...state,
            }
        }
        case FETCH_MORE_POSTS: {
            return {
                ...state,
                loading: true,
            }
        }
        case FETCH_MORE_POSTS_SUCCESS: {
            const { posts } = action.payload;
            return {
                ...state,
                feed: [...state.feed, ...posts],
                loading: false,
            }
        }
        case FETCH_MORE_POSTS_FAILED: {
            return {
                ...state,
                loading: false,
            }
        }
        default: 
            return state;
    }
}