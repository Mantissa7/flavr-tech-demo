import { combineReducers } from 'redux';
import { feedReducer } from './feed.reducer';
import { authReducer } from './auth.reducer';

export const rootReducer = combineReducers({
    auth: authReducer,
    feed: feedReducer,
});

export type AppState = ReturnType<typeof rootReducer>