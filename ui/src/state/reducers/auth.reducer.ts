import { AnyAction } from 'redux';
import { AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILED } from 'state/actions/types';

interface State {
    loading: boolean;
    user: any;
    token: string;
    error: string;
}

const initialState: State = {
    loading: false,
    user: null,
    token: null,
    error: null,
};

export function authReducer(state: State = initialState, action: AnyAction) {
    switch(action.type) {
        case AUTH_LOGIN: {
            return {
                ...state,
                loading: true,
                error: '',
            }
        }
        case AUTH_LOGIN_SUCCESS: {
            const { token, user } = action.payload;
            return {
                ...state,
                loading: false,
                user,
                token,
            }
        }
        case AUTH_LOGIN_FAILED: {
            const { reason } = action.payload;

            return {
                ...state,
                loading: false,
                error: reason,
            }
        }
        default: 
            return state;
    }
}