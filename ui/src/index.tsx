import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { makeStore } from './state';
import { HttpClient } from 'lib/http';
import { ServiceProvider } from 'lib/service-provider';
import { FeedService } from 'services/feed-service';
import { AuthService } from 'services/auth-service';
import { UserService } from 'services/user-service';


function configureDependencies() {
  
    const http = new HttpClient();
    const authService = new AuthService(http);
    const userService = new UserService(http);
    const feedService = new FeedService(http, userService);
  
    const deps: [string, () => any][] = [
      ['http', () => http],
      ['auth-service', () => authService],
      ['user-service', () => userService],
      ['feed-service', () => feedService],
    ];
  
    const di = new ServiceProvider();
  
    for (const [k, v] of deps) {
      di.set(k, v);
    }
  
    return di;
  }
  
  const di = configureDependencies()
  
  const store = makeStore(di);

ReactDOM.render(<Provider store={ store }><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
