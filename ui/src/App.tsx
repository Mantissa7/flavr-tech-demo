import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { Header } from 'components/Header/Header';
import { Feed } from 'views/Feed/Feed';
import './App.css';
import { Login } from 'views/Login/Login';
import { AuthGuard } from 'components/AuthGuard/AuthGuard';

const App: React.FC = () => {
  return (
    <div className="app">
      <Router>
        <Header />
        <main>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <AuthGuard path="/">
              <Feed />
            </AuthGuard>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
