import React, { ChangeEvent } from 'react';
import './input.scss';

interface InputProps {
    type?: string;
    label?: string;
    error?: string;
    value: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export function Input({ type, value, onChange, label, error }: InputProps) {
    return (
        <div className="input">
            <div className="input__label">{label}</div>
            <input type={type || 'text'} value={value} onChange={onChange}/>
            <div className="input__error">{error}</div>
        </div>
    )
}