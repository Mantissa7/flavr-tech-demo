import React from 'react';
import { Route, Redirect } from 'react-router';
import { useSelector } from 'react-redux';
import { AppState } from 'state/reducers';

export function AuthGuard({ children, ...rest }: any) {

    const auth = useSelector((state: AppState) => state.auth);

    return (
      <Route
        {...rest}
        render={({ location }) =>
          auth.token ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }