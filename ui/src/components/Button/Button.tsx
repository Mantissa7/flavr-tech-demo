import React from 'react';

interface ButtonProps {
    title: string;
    onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export function Button({ title, onClick }: ButtonProps) {
    return (
        <button onClick={onClick}>{title}</button>
    )
}