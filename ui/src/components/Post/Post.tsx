import React from 'react';
import './styles.scss';
import { User } from 'models/user';
import { Button } from 'components/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import { Like } from 'models/like';
import { likePost } from 'state/actions/feed.actions';
import { AppState } from 'state/reducers';

interface PostProps {
    id: number;
    message: string;
    created: string;
    user: User;
    likes: Like[],
}

export function Post({ id, message, created, user, likes }: PostProps) {

    const dispatch = useDispatch();
    const auth = useSelector((state: AppState) => state.auth);

    const handleLike = () => {
        dispatch(likePost(id))
    }

    return (
        <div className="post">
            <div className="post__icon">
                <div className="icon"></div>
            </div>

            <div className="post__user">{ user.forename } { user.surname }</div>
            <div className="post__message">{ message }</div>
            <div className="post__date">{ new Date(created).toLocaleDateString() }</div>
            <div className="post__meta">Likes: { likes.length }</div>
            <div className="post__actions">
                {
                    likes.find( like => like.user_id === auth.user.id) ? (
                        <div></div>
                    ) : (
                        <Button title="Like" onClick={ handleLike }/>
                    )
                }
            </div>
        </div>
    )
}