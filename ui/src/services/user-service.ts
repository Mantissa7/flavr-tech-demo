import { HttpClient } from 'lib/http';
import { Observable } from 'rxjs';
// import { User } from 'models/user';


export class UserService {

  private host: string = process.env.REACT_APP_API_URL;
  private base: string = 'api/v1';

  constructor(private http: HttpClient) { }// private storage: storage.Storage) { }

  public getUser(id: string): Observable<Response> {
    return this.http.get(`${this.host}/user/${id}`)
  }

}