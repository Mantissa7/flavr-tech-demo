import { HttpClient } from 'lib/http';
import { Observable } from 'rxjs';


export class AuthService {

  private host: string = process.env.REACT_APP_API_URL;
  private base: string = 'api/v1';

    constructor(private http: HttpClient) {}

    public login(email: string, password: string): Observable<Response> {
      return this.http.post(`${this.host}/${this.base}/auth/authorise`, {
        email, password
      });
    }

    public signup(email: string, password: string, username: string, fullname: string, nickname: string, photoURL: string) {
      const user = {
        email,
        password,
        username,
        fullname,
        nickname,
        photoURL,
      };

      return this.http.post(`${this.host}/auth/create`, user);
    }

}