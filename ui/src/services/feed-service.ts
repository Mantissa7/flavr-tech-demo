import { HttpClient } from 'lib/http';
import { UserService } from './user-service';
import { Observable } from 'rxjs';

export class FeedService {

  private host: string = process.env.REACT_APP_API_URL;
  private base: string = 'api/v1';

  constructor(private http: HttpClient, private userService: UserService) {}

  private headers() {
    const token = localStorage.getItem('access_token');
    return {
      'Authorization': `Bearer ${token}`,
    }
  }

  public fetchFeed(): Observable<any> {
    return this.http.get(`${this.host}/${this.base}/posts/`, {
      headers: this.headers(),
    })
  }

  public likePost(id: number): Observable<any> {
    return this.http.post(`${this.host}/${this.base}/posts/${id}/like`, {}, {
      headers: this.headers(),
    })
  }

  public submitPost(message: string, userID: string): Observable<Response> {
    return this.http.post(`${this.host}/${this.base}/posts/`, {
      message,
      userID,
    }, {
      headers: this.headers(),
    });
  }

}