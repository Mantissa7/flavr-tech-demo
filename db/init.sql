DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `forename` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `message` LONGTEXT NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `post_likes`;
CREATE TABLE `post_likes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY post_user (post_id , user_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `users` 
    (email, `password`, forename, surname, created, updated)
VALUES
    ('test@test.uk', '$2b$10$URZ5c3n6pRa3JJjCKzKMEOKYFGoi3qYejohyqJ8UpDQt.JcE5WFLu', 'Test', 'User', NOW(), NOW()),
    ('test2@test.uk', '$2b$10$URZ5c3n6pRa3JJjCKzKMEOKYFGoi3qYejohyqJ8UpDQt.JcE5WFLu', 'Another-Test', 'User', NOW(), NOW());


INSERT INTO `posts` 
    (user_id, `message`, created, updated)
VALUES
    (1, 'This is a test message', NOW(), NOW()),
    (2, 'This is another test message', NOW(), NOW());


INSERT INTO `post_likes` 
    (user_id, post_id, created, updated)
VALUES
    (1, 2, NOW(), NOW()),
    (2, 1, NOW(), NOW());