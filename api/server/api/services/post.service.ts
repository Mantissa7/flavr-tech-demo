import L from '../../common/logger';
import { getRepository } from 'typeorm';
import { Post } from '../../models/post.model';
import { User } from '../../models/user.model';
import { Like } from '../../models/like.model';

export class PostService {

  one(id: number) {
    return getRepository(Post)
      .find({
        id: id
      })
      .then(posts => {
        return posts.length > 0 ? posts[0] : null
      });
  }

  likers(id: number) {
    return getRepository(Like).find({
      post_id: id,
    });
  }

  like(post_id: number, user_id: number) {
    return getRepository(Like).insert({
      user_id: user_id,
      post_id: post_id,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    });
  }

  all() {
    return getRepository(Post).find();
  }

  create(message: string, user_id: number) {
    return getRepository(Post).insert({
      user_id,
      message,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    });
  }

}

export default new PostService();