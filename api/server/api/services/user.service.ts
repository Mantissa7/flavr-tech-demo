import L from '../../common/logger';
import bcrypt from 'bcrypt';
import { getRepository } from 'typeorm';
import { User } from '../../models/user.model';

interface UserRequest {
  [key: string]: string;
}

export class UserService {

  one(id: number) {
    return getRepository(User).findOneOrFail(id);
  }

  all() {
    return getRepository(User).find();
  }

  posts(id: number) {
    return getRepository(User).findOneOrFail(id).then((user) => user.posts);
  }
  

  create(user: UserRequest) {
    return Promise.resolve(true);
  }

  private createHash(password: string) {
    return new Promise((res, rej) => {
      bcrypt.hash(password, 10, function (err, hash) {
        if (err) {
          rej(err);
        }
        res(hash);
      });

    })
  }

}

export default new UserService();