import L from '../../common/logger';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { User } from '../../models/user.model';

class InvalidCredentialsError extends Error {
  constructor(message) {
    super(message);
    this.name = "InvalidCredentialsError";
  }
}

class TokenGenerationError extends Error {
  constructor(message) {
    super(message);
    this.name = "TokenGenerationError";
  }
}

export class AuthService {

  authenticate(token: string): boolean | {[key: string]: string} {
    L.info('Authenticating User');

    try {
      return jwt.verify(token, process.env.JWT_SECRET)
    } catch (err) {
      L.info(err.message);
    }

    return false;
  }

  async authorise(email: string, password: string): Promise<[string, User]> {
    L.info('Authorising User');
    const user = await getRepository(User).findOneOrFail({
      email,
    });

    const ok = await this.verifyHash(password, user.password);
    if (!ok) {
      throw new InvalidCredentialsError('Missing or Incorrect Login Details');
    }

    try {
      const token = jwt.sign({
        id: user.id,
        email: user.email,
      }, process.env.JWT_SECRET);
      return [token, user];
    } catch (err) {
      throw new TokenGenerationError('Something went wrong generating a token');
    }
  }

  private createHash(password: string) {
    return new Promise((res, rej) => {
      bcrypt.hash(password, 10, function (err, hash) {
        if (err) {
          rej(err);
        }
        res(hash);
      });

    })
  }

  private verifyHash(pass: string, hash: string): Promise<boolean> {
    return new Promise((res, rej) => {
      bcrypt.compare(pass, hash, function (err, ok: boolean) {
        if (err) {
          rej(err);
        }
        res(ok);
      });
    })
  }

}

export default new AuthService();