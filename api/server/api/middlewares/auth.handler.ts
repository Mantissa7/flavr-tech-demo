import { Request, Response, NextFunction } from 'express';
import AuthService from '../services/auth.service';

export function authHandler(req: Request, res: Response, next: NextFunction) {
  const auth = req.headers.authorization;
  if(!auth || !auth.length) {
    res.status(401).end();
    return;
  }
  const token = auth.substr(7);
  const payload = AuthService.authenticate(token);
  if(payload) {
    req['user'] = payload;
    next();
  } else {
    res.status(401).end();
  }
}

