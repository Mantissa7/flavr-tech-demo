import express from 'express';
import controller from './controller'
export default express.Router()
    .post('/authorise', controller.authorise)
    .post('/authenticate', controller.authenticate);