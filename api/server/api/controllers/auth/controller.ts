import L from '../../../common/logger';
import AuthService from '../../services/auth.service';
import { Request, Response } from 'express';

export class Controller {

  async authenticate(req: Request, res: Response): Promise<void> {
    const auth = req.headers.authorization;
    const token = auth.substr(7);
    const success = await AuthService.authenticate(token);
    res.json({success});
  }

  async authorise(req: Request, res: Response): Promise<void> {
    const { email, password } = JSON.parse(req.body);

    try {
      const [token, user] = await AuthService.authorise(email, password);
      res.json({
        token,
        user
      });
    } catch(err) {
      switch(err.name) {
        case 'InvalidCredentialsError':
          res.status(400).json({error: 'Invalid Credentials'});
          break;
        case 'TokenGenerationError':
        default:
          L.info(err.name)
          res.status(500).json({error: 'Something Went Wrong'});
          break;
      }
    }
    
  }
  
}
export default new Controller();
