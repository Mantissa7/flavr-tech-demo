import L from '../../../common/logger';
import UserService from '../../services/user.service';
import { Request, Response } from 'express';

export class Controller {

  async one(req: Request, res: Response) {
    const id = +req.params.id;
    const user = await UserService.one(id);
    res.json(user);
  }

  async all(req: Request, res: Response) {
    const users = await UserService.all();
    res.json(users);
  }

  async posts(req: Request, res: Response) {
    const id = +req.params.id;
    const posts = await UserService.posts(id);
    res.json(posts);
  }

  create(req: Request, res: Response): void {
    // UserService.create(req.body).then(r =>
    //   res
    //     .status(201)
    //     .location(`/api/v1/examples/${ r.id }`)
    //     .json(r),
    // );
    res.json({err: 'NOT Implemented'})
  }
}
export default new Controller();
