import L from '../../../common/logger';
import PostService from '../../services/post.service';
import { Request, Response } from 'express';

export class Controller {

  async one(req: Request, res: Response) {
    const id = +req.params.id;

    try {
      const post = await PostService.one(id);
      res.json(post);
    } catch(err) {
      L.info(err.message)
    }


    // res.json({
    //   id: post.id,
    //   message: post.message,
    //   created: post.created,
    // });
    res.status(500).send();
  }

  async like(req: Request, res: Response) {
    const user = req['user'].id;
    const id = +req.params.id;
    try {
      await PostService.like(id, user);
      res.status(201).send();
    } catch(err) {
      res.status(500).send();
    }
  }

  async likers(req: Request, res: Response) {
    const id = +req.params.id;
    const likers = await PostService.likers(id);
    res.json(likers);
  }

  async all(req: Request, res: Response) {
    const posts = await PostService.all();
    // const likes = posts.likes
    // res.json(posts);
    res.json(posts.map( post => {
      return {
        id: post.id,
        message: post.message,
        created: post.created,
        user: {
          forename: post.user.forename,
          surname: post.user.surname,
        },
        likes: post.likes,
      }
    }));
  }

  async create(req: Request, res: Response): Promise<void> {
    const userID = req['user'].id;
    const { message } = JSON.parse(req.body);

    try {
      const done = await PostService.create(message, userID);
      res.json({done})
    } catch(err) {
      L.info(err.message);
    }

    res.status(500).send();
  }
}
export default new Controller();
