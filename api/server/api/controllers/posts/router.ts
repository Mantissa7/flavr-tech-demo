import express from 'express';
import controller from './controller'

export default express.Router()
    .get('/', controller.all)
    .get('/:id', controller.one)
    .get('/:id/likers', controller.likers)
    .post('/:id/like', controller.like)
    .post('/', controller.create)