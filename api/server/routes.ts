import { Application } from 'express';
import authRouter from './api/controllers/auth/router';
import userRouter from './api/controllers/user/router';
import postsRouter from './api/controllers/posts/router';
import { authHandler } from './api/middlewares/auth.handler';

export default function routes(app: Application): void {
  const prefix = '/api/v1';
  app.use(`${prefix}/auth`, authRouter);
  app.use(`${prefix}/user`, authHandler, userRouter);
  app.use(`${prefix}/posts`, authHandler, postsRouter);
}
