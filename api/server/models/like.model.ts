import {Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Index} from "typeorm";
import { Post } from './post.model';

@Entity('post_likes')
@Index(['post_id', 'user_id'], { unique: true })
export class Like {
    
    @PrimaryGeneratedColumn({type: 'bigint'})
    id: number;

    @Column('bigint')
    user_id: number;

    @Column('bigint')
    post_id: number;

    @Column('datetime')
    created: string;
    
    @Column('datetime')
    updated: string;

    @ManyToOne(type => Post, post => post.likes)
    @JoinColumn({ name: 'post_id' })
    post: Post;

}