import {Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import { User } from './user.model';
import { Like } from './like.model';

@Entity('posts')
export class Post {
    
    @PrimaryGeneratedColumn({type: 'bigint'})
    id: number;

    @Column({ type: 'bigint', unique: false})
    user_id: number;

    @Column('longtext')
    message: string;

    @Column('datetime')
    created: string;
    
    @Column('datetime')
    updated: string;

    @OneToOne(type => User, {eager: true})
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(() => User, (author: User) => author.posts)
    @JoinColumn({ name: 'user_id' })
    public author: User;

    @OneToMany(type => Like, like => like.post, {eager: true})
    likes: Like[];

}