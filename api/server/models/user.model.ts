import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import { Post } from './post.model';

@Entity('users')
export class User {
    
    @PrimaryGeneratedColumn({type: 'bigint'})
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    forename: string;
    
    @Column()
    surname: string;

    @Column('datetime')
    created: string;

    @Column('datetime')
    updated: string;

    @OneToMany(() => Post, (post: Post) => post.author)
    public posts: Promise<Post[]>;

}