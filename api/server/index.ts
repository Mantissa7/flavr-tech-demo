import './common/env';
import 'reflect-metadata';
import Server from './common/server';
import routes from './routes';
import { createConnection } from 'typeorm';

const port = parseInt(process.env.PORT);

createConnection()
    .then(connection => {
        new Server().router(routes).listen(port);
    })
    .catch( err => {
        console.warn('MYSQL Error');
        console.log(err);
    });
// export default new Server().router(routes).listen(port);
